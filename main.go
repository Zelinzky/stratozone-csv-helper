package main

import (
	"os"

	"gitlab.com/zelinzky/stratozone-csv-helper/internal/commands"
)

func main() {
	if err := commands.NewDefaultCommand().Execute(); err != nil {
		os.Exit(1)
	}
}
