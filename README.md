# Stratozone CSV helper
[![pipeline status](https://gitlab.com/Zelinzky/stratozone-csv-helper/badges/master/pipeline.svg)](https://gitlab.com/Zelinzky/stratozone-csv-helper/-/commits/master) [![Go Report Card](https://goreportcard.com/badge/gitlab.com/Zelinzky/stratozone-csv-helper)](https://goreportcard.com/report/gitlab.com/Zelinzky/stratozone-csv-helper)

This little app help you create csv file for the stratozone collectors.
It may be useful when you've got to do something different but similar 
to the bulk scan. Or when your infrastructure is mutable.

## Usage

Just download the repo, make sure to have go installed and just do
```go build``` inside the folder.

After that you'll get an executable called as your folder

The executable has 4 flags, to know more run 
```./executableName -help```

## Download

The windows and linux executables can be downloaded directly from the latest successful pipeline.