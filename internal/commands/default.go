package commands

import (
	"os"
	"path"

	"github.com/spf13/cobra"
)

func NewDefaultCommand() *cobra.Command {
	cmd := cobra.Command{
		Use:   path.Base(os.Args[0]),
		Short: "This app helps you create csv in correct format for stratozone",
		Long: `Stratozone scanner app accepts bulk csv files with lists of hosts ip in order to scan them.
This app helps to create said csv files in the correct format from a cidr or a list of cidr`,
	}

	cmd.AddCommand(newGenerateCommand())

	return &cmd
}
