package commands

import (
	"net"
	"reflect"
	"testing"
)

func Test_incrementIp(t *testing.T) {
	tests := []struct {
		name     string
		input    net.IP
		expected net.IP
	}{
		{name: "Increment last byte", input: net.ParseIP("192.168.0.1"), expected: net.ParseIP("192.168.0.2")},
		{name: "Increment last 2 bytes", input: net.ParseIP("192.168.0.255"), expected: net.ParseIP("192.168.1.0")},
		{name: "Increment last 3 bytes", input: net.ParseIP("192.168.255.255"), expected: net.ParseIP("192.169.0.0")},
		{name: "Increment all bytes", input: net.ParseIP("192.255.255.255"), expected: net.ParseIP("193.0.0.0")},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			incrementIp(&tt.input)
			if result := tt.input; !tt.expected.Equal(result) {
				t.Errorf("%s : FAILED, expected %v and got %v", tt.name, tt.expected, result)
			}
		})
	}
}

func Test_cidrsToHosts(t *testing.T) {
	tests := []struct {
		name      string
		cidrs     []string
		expected  []string
		expectErr bool
	}{
		{name: "pass multiple cidr", cidrs: []string{"232.32.25.21/32", "232.34.24.21/32", "231.32.24.21/32"}, expected: []string{"232.32.25.21", "232.34.24.21", "231.32.24.21"}, expectErr: false},
		{name: "pass invalid cidr 1", cidrs: []string{"232.32.25.21/334"}, expected: nil, expectErr: true},
		{name: "pass invalid cidr 2", cidrs: []string{"1232.32.24.21*26"}, expected: nil, expectErr: true},
		{name: "pass no cidr", cidrs: []string{}, expected: nil, expectErr: false},
		{name: "/32 cidr returns single host", cidrs: []string{"232.32.24.21/32"}, expected: []string{"232.32.24.21"}, expectErr: false},
		{name: "/31 cidr returns no host", cidrs: []string{"232.32.24.21/31"}, expected: nil, expectErr: false},
		{name: "/29 cidr returns 6 host", cidrs: []string{"232.32.24.21/29"}, expected: []string{"232.32.24.17", "232.32.24.18", "232.32.24.19", "232.32.24.20", "232.32.24.21", "232.32.24.22"}, expectErr: false},
		{name: "/30 cidr returns 2 hosts", cidrs: []string{"232.32.24.21/30"}, expected: []string{"232.32.24.21", "232.32.24.22"}, expectErr: false},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			actual, err := cidrsToHosts(tt.cidrs)
			if (err != nil) != tt.expectErr {
				t.Errorf("%s : FAILED expected error = %v, actual error = %v", tt.name, tt.expectErr, err)
				return
			}
			if !reflect.DeepEqual(actual, tt.expected) {
				t.Errorf("%s : FAILED expected %v and actual %v", tt.name, tt.expected, actual)
			}
		})
	}
}
