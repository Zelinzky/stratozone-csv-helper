package commands

import (
	"bufio"
	"errors"
	"fmt"
	"net"
	"os"

	"github.com/spf13/cobra"
	"github.com/spf13/viper"
)

func newGenerateCommand() *cobra.Command {
	cmd := cobra.Command{
		Use:       "generate <import-asset|bulk-scan>",
		Short:     "Creates stratozone csv file",
		Args:      cobra.ExactValidArgs(1),
		ValidArgs: []string{"import-asset", "bulk-scan"},
		PreRunE: func(cmd *cobra.Command, args []string) error {
			if err := viper.BindPFlag("cidrs", cmd.Flags().Lookup("cidrs")); err != nil {
				return fmt.Errorf("bind cidrs flag: %w", err)
			}

			if err := viper.BindPFlag("file", cmd.Flags().Lookup("file")); err != nil {
				return fmt.Errorf("bind file flag: %w", err)
			}

			if (len(viper.GetStringSlice("cidr")) == 0) != (viper.GetString("file") == "") {
				return errors.New("one flag (cidr or file, not both) must be set")
			}

			if err := viper.BindPFlag("out", cmd.Flags().Lookup("out")); err != nil {
				return fmt.Errorf("bind out flag: %w", err)
			}

			if err := viper.BindPFlag("type", cmd.Flags().Lookup("type")); err != nil {
				return fmt.Errorf("bind type flag: %w", err)
			}

			if viper.GetString("type") != "linux" && viper.GetString("type") != "windows" {
				return errors.New("type of machines can only be either windows or linux")
			}

			return nil
		},
		RunE: func(cmd *cobra.Command, args []string) error {
			if err := runGenerate(args[0]); err != nil {
				return fmt.Errorf("run generate: %w", err)
			}

			return nil
		},
	}

	cmd.Flags().StringSlice("cidrs", []string{}, "cidrs from which to extract hosts for stratozone csv")
	cmd.Flags().StringP("file", "f", "",
		"path or name of the file that contains a list of cidrs")
	cmd.Flags().StringP("out", "o", "importAsset.csv", "name for the output csv file")
	cmd.Flags().StringP("type", "t", "linux", "type of machines, enter windows or linux")

	return &cmd
}

func runGenerate(formatType string) error {
	var cidrs = viper.GetStringSlice("cidrs")
	if len(cidrs) == 0 {
		var err error
		cidrs, err = getCidrsFromFile(viper.GetString("file"))
		if err != nil {
			return fmt.Errorf("read lines: %w", err)
		}
	}

	hosts, err := cidrsToHosts(cidrs)
	if err != nil {
		return fmt.Errorf("cidrs to hosts: %w", err)
	}

	f, err := os.Create(viper.GetString("out"))
	if err != nil {
		return fmt.Errorf("create output file: %w", err)
	}
	defer f.Close()

	var hostType int
	if viper.GetString("type") == "linux" {
		hostType = 1
	}

	formatTypeString := "host%d,%s,,%d\n"
	if formatType == "bulk-scan" {
		formatTypeString = "%s\n"
	}

	for i, host := range hosts {
		line := fmt.Sprintf(formatTypeString, i, host, hostType)
		if _, err := f.WriteString(line); err != nil {
			return fmt.Errorf("write output line: %w", err)
		}
	}

	if err = f.Close(); err != nil {
		return fmt.Errorf("close output file: %w", err)
	}

	fmt.Printf("success: file %s created\n", f.Name())

	return nil
}

func getCidrsFromFile(path string) ([]string, error) {
	file, err := os.Open(path)
	if err != nil {
		return nil, fmt.Errorf("open file: %w", err)
	}
	defer file.Close()

	var lines []string
	scanner := bufio.NewScanner(file)
	for scanner.Scan() {
		lines = append(lines, scanner.Text())
	}

	if err := scanner.Err(); err != nil {
		return nil, fmt.Errorf("scanning file: %w", err)
	}

	return lines, nil
}

func cidrsToHosts(cidrs []string) ([]string, error) {
	var completeHosts []string
	for _, cidr := range cidrs {
		ip, ipNet, err := net.ParseCIDR(cidr)
		if err != nil {
			return nil, fmt.Errorf("parse cidr: %w", err)
		}

		var ips []string
		for ip := ip.Mask(ipNet.Mask); ipNet.Contains(ip); incrementIp(&ip) {
			ips = append(ips, ip.String())
		}

		lenIPs := len(ips)
		// Remove net address and broadcast address as those addresses
		// do not correspond to valid hosts in the network.
		if lenIPs >= 2 {
			ips = ips[1 : lenIPs-1]
		}

		completeHosts = append(completeHosts, ips...)
	}

	return completeHosts, nil
}

func incrementIp(ip *net.IP) {
	for j := len(*ip) - 1; j > 0; j-- {
		(*ip)[j]++
		if (*ip)[j] > 0 {
			break
		}
	}
}
