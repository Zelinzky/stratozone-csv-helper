module gitlab.com/zelinzky/stratozone-csv-helper

go 1.14

require (
	github.com/spf13/cobra v1.0.0
	github.com/spf13/viper v1.4.0
)
